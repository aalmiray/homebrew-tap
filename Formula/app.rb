# Generated with JReleaser 1.1.0 at 2022-07-13T11:20:22.687+02:00
class App < Formula
  desc "Sample application"
  homepage "https://acme.com/wesome-app"
  url "https://gitlab.com/aalmiray/app/-/releases/v1.0.0/downloads/app-1.0.0.zip"
  version "1.0.0"
  sha256 "b839f34b9581aabaf27bf367cecb04570285ce2d1ff7f5df9a2eecbc19158322"
  license "Apache-2.0"

  depends_on "openjdk@8"

  def install
    libexec.install Dir["*"]
    bin.install_symlink "#{libexec}/bin/app" => "app"
  end

  test do
    output = shell_output("#{bin}/app --version")
    assert_match "1.0.0", output
  end
end
