# Generated with JReleaser 1.0.0-M1 at 2022-01-27T02:58:57.005+01:00
class App < Formula
  desc "Sample application"
  homepage "https://acme.com/wesome-app"
  url "https://codeberg.org/aalmiray/app/releases/download/v1.0.0/app-1.0.0.zip"
  version "1.0.0"
  sha256 "81afd20bdf3f6f513f028691a22d9fa7c5a24f003eefe33878e60262c0fb153f"
  license "Apache-2.0"

  depends_on "openjdk@8"

  def install
    libexec.install Dir["*"]
    bin.install_symlink "#{libexec}/bin/app"
  end

  test do
    output = shell_output("#{bin}/app --version")
    assert_match "1.0.0", output
  end
end
